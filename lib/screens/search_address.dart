
import 'package:flutter/material.dart';
import 'package:latlong/latlong.dart';
import 'package:mds_flutter_correction/models/address.dart';
import 'package:mds_flutter_correction/repository/address_repository.dart';
import 'package:mds_flutter_correction/repository/repository.dart';
import 'package:provider/provider.dart';

class SearchAddress extends StatefulWidget {

  @override
  _SearchAddressState createState() => _SearchAddressState();
}

class _SearchAddressState extends State<SearchAddress> {

  List<Address> _addresses = [];

  @override
  Widget build(BuildContext context) {
    Repository repository = Provider.of<Repository>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Recherche adresse'),
      ),
      body: Container(
        child: Column(
          children: [
            TextField(
              autofocus: true,
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.place),
                  labelText: "Adresse de l'entreprise"),
              onChanged: (query) async {
                if(query.length >= 3) {
                  List<Address> addresses = await repository.searchAddresses(query);
                  if (addresses != null) {
                    setState(() {
                      _addresses = addresses;
                    });
                  }
                } else {
                  setState(() {
                    _addresses = [];
                  });
                }
              },
            ),
            Expanded(
              child: ListView.separated(
                itemCount: _addresses.length,
                itemBuilder: (BuildContext context, int index) {
                  Address address = _addresses[index];
                  return ListTile(
                    leading: Icon(Icons.place),
                    title: Text(address.fullName()),
                    subtitle: Text("[${address.position.latitude};${address.position.longitude}]"),
                    onTap: () {
                      Navigator.of(context).pop(address);
                    },
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return Divider(
                    height: 0,
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
