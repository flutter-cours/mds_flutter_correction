import 'package:flutter/material.dart';
import 'package:mds_flutter_correction/models/address.dart';
import 'package:mds_flutter_correction/stores/company_store.dart';
import 'package:provider/provider.dart';
import '../models/company.dart';
import '../routes.dart';

class AddCompany extends StatelessWidget {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _addressController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey();
  Address _address;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Ajouter une entreprise"),
        ),
        body: Container(
          margin: EdgeInsets.symmetric(vertical: 20, horizontal: 15),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                TextFormField(
                  validator: (value) => value.isEmpty ? 'Le nom ne doit pas être vide' : null,
                  controller: _nameController,
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.apartment),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15)),
                      labelText: "Nom de l'entreprise"),
                ),
                SizedBox(height: 10,),
                TextFormField(
                  validator: (value) => value.isEmpty ? "L'adresse ne doit pas être vide" : null,
                  controller: _addressController,
                  readOnly: true,
                  onTap: () async {
                    _address = await Navigator.of(context).pushNamed(Routes.searchAddress) as Address;
                    if(_address != null) {
                      _addressController.text = _address.fullName();
                    }
                  },
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.place),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15)),
                      labelText: "Adresse de l'entreprise"),
                ),
                Container(
                    margin: EdgeInsets.symmetric(vertical: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        OutlinedButton.icon(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            label: Text('Annuler'),
                            icon: Icon(Icons.cancel_outlined)),
                        SizedBox(
                          width: 10,
                        ),
                        ElevatedButton.icon(
                          onPressed: () {

                            if(this._formKey.currentState.validate()) {
                              String name = this._nameController.text;
                              Company company = Company(0, name, _address);

                              Provider.of<CompanyStore>(context, listen: false).addCompany(company);

                              Navigator.pop(context);
                            }

                          },
                          label: Text('Valider'),
                          icon: Icon(Icons.check),
                        ),
                      ],
                    ))
              ],
            ),
          ),
        ));
  }
}
