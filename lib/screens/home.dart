import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mds_flutter_correction/stores/company_store.dart';
import 'package:mds_flutter_correction/tabs/list_tab.dart';
import 'package:mds_flutter_correction/tabs/map_tab.dart';
import 'package:provider/provider.dart';

import '../routes.dart';

class Home extends StatefulWidget {

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  int _index = 0;

  @override
  void didChangeDependencies() {
    Provider.of<CompanyStore>(context).loadCompanies();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          title: Text("MyDigitalMap"),
        ),
        body: IndexedStack(
          index: _index,
          children: [
            MapTab(),
            ListTab()
          ],
        ),
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _index,
          onTap: (index) {
            setState(() {
              _index = index;
            });
          },
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.place), label: 'Carte'),
            BottomNavigationBarItem(icon: Icon(Icons.storage), label: 'Liste'),
          ]
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pushNamed(Routes.addCompany);
        },
        child: Observer(builder: (context) {
          return Text(Provider.of<CompanyStore>(context).companies.length.toString());
        }),
      ),
    );
  }
}


