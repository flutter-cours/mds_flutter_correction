import 'package:flutter/material.dart';
import 'package:mds_flutter_correction/repository/address_repository.dart';
import 'package:mds_flutter_correction/repository/preferences_repository.dart';
import 'package:mds_flutter_correction/repository/repository.dart';
import 'package:mds_flutter_correction/screens/search_address.dart';
import 'package:mds_flutter_correction/stores/company_store.dart';
import 'package:provider/provider.dart';
import 'routes.dart';
import 'screens/add_company.dart';
import 'screens/home.dart';

void main() {

  PreferencesRepository preferencesRepository = PreferencesRepository();
  AddressRepository addressRepository = AddressRepository();

  Repository repository = Repository(addressRepository, preferencesRepository);
  CompanyStore companyStore = CompanyStore(repository);

  runApp(
      MultiProvider(
        providers: [
          Provider<CompanyStore>(create: (_) => companyStore),
          Provider<Repository>(create: (_) => repository),
        ],
        child: MyApp(),
      )
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      routes: {
        Routes.addCompany: (context) => AddCompany(),
        Routes.searchAddress: (context) => SearchAddress()
      },
      home: Home(),
    );
  }
}

