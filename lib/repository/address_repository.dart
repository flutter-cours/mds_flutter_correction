import 'dart:convert';

import 'package:http/http.dart';
import 'package:mds_flutter_correction/models/address.dart';

class AddressRepository {

  Future<List<Address>> fetchAddresses(String query) async {
    Response response = await get('https://api-adresse.data.gouv.fr/search/?q=$query&limit=15');
    if(response.statusCode == 200) {
      List<Address> addresses = [];
      Map<String, dynamic> json = jsonDecode(response.body);
      if(json.containsKey("features")) {
        List<dynamic> features = json['features'];
        features.forEach((feature) {
          addresses.add(Address.fromGeoJson(feature));
        });
// ...
      }
      return addresses;
    } else {
      throw Exception('Failed to load addresses');
    }
  }

}