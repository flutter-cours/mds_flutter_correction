import 'package:mds_flutter_correction/models/company.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferencesRepository {

  Future<void> saveCompanies(List<Company> companies) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> strings = [];

    companies.forEach((company) {
      strings.add(company.toJson());
    });

    prefs.setStringList('companies', strings);

  }

  Future<List<Company>> loadCompanies() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    List<Company> companies = [];

    prefs.getStringList('companies')?.forEach((companyString) {
      companies.add(Company.fromJson(companyString));
    });

    return companies;
  }
}