import 'package:mds_flutter_correction/models/address.dart';
import 'package:mds_flutter_correction/models/company.dart';
import 'package:mds_flutter_correction/repository/address_repository.dart';
import 'package:mds_flutter_correction/repository/preferences_repository.dart';

class Repository {
  AddressRepository _addressRepository;
  PreferencesRepository _preferencesRepository;

  Repository(this._addressRepository, this._preferencesRepository);

  Future<List<Address>> searchAddresses(String query) =>
      this._addressRepository.fetchAddresses(query);

  Future<void> saveCompanies(List<Company> companies) async {
    this._preferencesRepository.saveCompanies(companies);
  }

  Future<List<Company>> loadCompanies() async =>
      this._preferencesRepository.loadCompanies();
}
