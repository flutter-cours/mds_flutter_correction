import 'dart:convert';

import 'package:mds_flutter_correction/models/address.dart';

class Company {
  int id;
  String name;
  Address address;


  Company(this.id, this.name, this.address);

  String toJson() {
    return jsonEncode({
      'id': this.id,
      'name' : this.name,
      'address': this.address.toJson()
    });
  }
  factory Company.fromJson(String json) {
    Map<String, dynamic> map = jsonDecode(json);
    return Company(
      map['id'] as int,
      map['name'] as String,
      Address.fromJson(map['address'])
    );
  }
}