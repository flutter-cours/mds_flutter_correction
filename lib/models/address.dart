import 'dart:convert';

import 'package:latlong/latlong.dart';

class Address {
  String street;
  String city;
  String postcode;
  LatLng position;

  Address(this.street, this.city, this.postcode, this.position);

  factory Address.fromGeoJson(Map<String, dynamic> json) {
    Map<String, dynamic> properties = json['properties'] ?? {};
    String street = properties['name'];
    String city = properties['city'];
    String postcode = properties['postcode'];

    Map<String, dynamic> geometry = json['geometry'] ?? {};
    List<dynamic> coordinates = geometry['coordinates'] ?? [];

    double latitude = coordinates[1];
    double longitude = coordinates[0];
    LatLng position = LatLng(latitude, longitude);
// ...
    return Address(street, city, postcode, position);
  }

  String toJson() {
    return jsonEncode({
      'street': this.street,
      'city': this.city,
      'postcode': this.postcode,
      'latitude': this.position.latitude,
      'longitude': this.position.longitude,
    });
  }

  factory Address.fromJson(String json) {
    Map<String, dynamic> map = jsonDecode(json);
    return Address(
        map['street'] as String,
        map['city'] as String,
        map['postcode'] as String,
        LatLng(map['latitude'] as num, map['longitude'] as num)
    );
  }

  String fullName() {
    return "$street, $postcode $city";
  }
}
