import 'package:mds_flutter_correction/models/company.dart';
import 'package:mds_flutter_correction/repository/repository.dart';
import 'package:mobx/mobx.dart';

part 'company_store.g.dart';

class CompanyStore = _CompanyStore with _$CompanyStore;

abstract class _CompanyStore with Store {
  final Repository _repository;

  @observable
  ObservableList<Company> companies = ObservableList<Company>();

  _CompanyStore(this._repository);

  @action
  void addCompany(Company company) {
    companies.add(company);
    this._repository.saveCompanies(companies);
  }

  @action
  Future<void> loadCompanies() async {
    companies.clear();
    companies.addAll(await this._repository.loadCompanies());
  }
}
