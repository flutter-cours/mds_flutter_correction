// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'company_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CompanyStore on _CompanyStore, Store {
  final _$companiesAtom = Atom(name: '_CompanyStore.companies');

  @override
  ObservableList<Company> get companies {
    _$companiesAtom.reportRead();
    return super.companies;
  }

  @override
  set companies(ObservableList<Company> value) {
    _$companiesAtom.reportWrite(value, super.companies, () {
      super.companies = value;
    });
  }

  final _$loadCompaniesAsyncAction = AsyncAction('_CompanyStore.loadCompanies');

  @override
  Future<void> loadCompanies() {
    return _$loadCompaniesAsyncAction.run(() => super.loadCompanies());
  }

  final _$_CompanyStoreActionController =
      ActionController(name: '_CompanyStore');

  @override
  void addCompany(Company company) {
    final _$actionInfo = _$_CompanyStoreActionController.startAction(
        name: '_CompanyStore.addCompany');
    try {
      return super.addCompany(company);
    } finally {
      _$_CompanyStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
companies: ${companies}
    ''';
  }
}
