import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mds_flutter_correction/models/company.dart';
import 'package:mds_flutter_correction/stores/company_store.dart';
import 'package:provider/provider.dart';

class ListTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    CompanyStore store = Provider.of<CompanyStore>(context);
    return Container(
        child: Observer(
          builder: (context) {
            return ListView.separated(
            itemCount: store.companies.length,
            itemBuilder: (BuildContext context, int index) {
              Company company = store.companies[index];
              return ListTile(
                leading: Icon(Icons.work_outlined),
                title: Text(company?.name ?? 'Entreprise inconnue'),
                subtitle: Text(company?.address?.fullName() ?? ''),
                onTap: () => print('tap $index ${company.name}'),
              );
            },
            separatorBuilder: (BuildContext context, int index) {
              return Divider(
                height: 0,
              );
            },
          );
          },
        ));
  }
}
