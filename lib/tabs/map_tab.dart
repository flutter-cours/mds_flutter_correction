import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:latlong/latlong.dart';
import 'package:mds_flutter_correction/models/company.dart';
import 'package:mds_flutter_correction/stores/company_store.dart';
import 'package:provider/provider.dart';

class MapTab extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    CompanyStore store = Provider.of<CompanyStore>(context);
    return Observer(
      builder: (context) => new FlutterMap(
        options: new MapOptions(
          center: new LatLng(47.469925, -0.552618),
          zoom: 13.0,
        ),
        layers: [
          new TileLayerOptions(
              urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
              subdomains: ['a', 'b', 'c']
          ),
          new MarkerLayerOptions(
            markers: _markers(store.companies) ,
          ),
        ],
      ),
    );
  }

  List<Marker> _markers(List<Company> companies) {
    List<Marker> markers = [];
    companies.forEach((company) {
      if(company.address?.position != null) {
        markers.add(Marker(
          width: 80.0,
          height: 80.0,
          point: company.address.position,
          builder: (ctx) =>
          new Container(
            child: Icon(Icons.place, color: Colors.red, size: 35,),
          ),
        ));
      }
    });
    return markers;
  }
}
